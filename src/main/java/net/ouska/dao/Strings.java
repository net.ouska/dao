package net.ouska.dao;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Strings {

	
	public static void main(String[] args) {
		System.out.println( stripAccents("Ouška"));
	}

	public static String nullToEmpty(String string) {
		return string != null ? string : ""; 
	}

	public static List<String> filterNotBlank(String ... values) {
		List<String> result = new ArrayList<>();
		for (String string : ArrayIterable.of(values)) {
			if (isNotBlank(string)) {
				result.add(string);
			}
		}  
	    return result;
	}

	public static String joinNotBlank(String delim, String ... values) {
	    return filterNotBlank(values).stream().collect(Collectors.joining(delim));
	}

	public static String appendIfNotBlank(String string, String appendix) {
		return string != null && isBlank(string)? string  + appendix : string; 
	}

	public static String prependIfNotBlank(String string, String prependix) {
		return string != null && isBlank(string)? prependix + string : string; 
	}

	public static String nvlOrEmpty(String ... strings) {
		for (String string : ArrayIterable.of(strings)) {
			if (string != null) {
				return string;
			}
		}  
	    return "";
	}

	public static String nbl(String ... strings) {
		for (String string : ArrayIterable.of(strings)) {
			if (isNotBlank(string)) {
				return string;
			}
		}  
	    return "";
	}

	public static String normalizeRegistrationNumber(String s) {
	    if (!isEmpty(s)) {
		    s = normalize(s).replaceAll("\\\\", "").replaceAll("/", "").trim();
	    }
	    return s;
	}

	public static String normalize(String s) {
	    if (!isEmpty(s)) {
		    s = stripAccents(s.toLowerCase());
	    }
	    return s;
	}

	public static String normalizeISIN(String isin) {
	    if (!isEmpty(isin)) {
	    	isin = stripAccents(isin.toUpperCase()).trim();
	    }
	    return isin;
	}

	public static String stripAccents(String s) {
	    if (!isEmpty(s)) {
			s = Normalizer.normalize(s, Normalizer.Form.NFD);
		    s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
	    }
	    return s;
	}
	
	public static String compareUnlocalizedString(String s) {
	    s = Normalizer.normalize(s, Normalizer.Form.NFD);
	    s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
	    return s;
	}

	public static boolean isEmpty(String s) {
	    return s == null || s.isEmpty();
	}

	public static boolean isNotEmpty(String s) {
	    return ! isEmpty(s);
	}

	public static boolean isBlank(String s) {
	    return s == null || s.trim().isEmpty();
	}

	public static boolean isNotBlank(String s) {
	    return ! isBlank(s);
	}

	public static boolean isAllBlank(String ... s) {
	    boolean isAllBlank = true; 
		if (s!=null) {
	    	for (String string : s) {
				if (!isBlank(string)) {
					return false;
				}
			}
	    }
		return isAllBlank;
	}

	public static String left(String s, int maxLength) {
		return s != null
			? s.substring(0, Math.min(s.length(), maxLength))
			: null;
	}

	public static boolean containsUnlocalized(String haystack, String needle) {
	    if (isBlank(haystack) || isBlank(needle)) {
	    	return false;
	    }
		haystack = stripAccents(haystack.toLowerCase());
	    needle = stripAccents(needle.toLowerCase());
	    return haystack.contains(needle);
	}

}
