package net.ouska.dao;

public interface LogicalOperation {

	public String apply();
	
	static LogicalOperation nope() {
		return () -> "";
	}

	static LogicalOperation and() {
		return () -> " AND ";
	}

	static LogicalOperation or() {
		return () -> " OR ";
	}

	static LogicalOperation openBlock() {
		return () -> " ( ";
	}

	static LogicalOperation closeBlock() {
		return () -> " ) ";
	}

	static LogicalOperation not() {
		return () -> " NOT ";
	}

}
