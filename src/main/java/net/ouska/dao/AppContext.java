package net.ouska.dao;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class AppContext implements ApplicationContextAware {
	
	private volatile static ApplicationContext applicationContext;
	 
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		applicationContext = appContext;
	}

	public static ApplicationContext getContext() {
		return applicationContext;
	}
	
	public static <T> T getBean(Class<T> clazz) {
		return (T) applicationContext.getBean(clazz.getSimpleName());
	}
	
	public static <T> T getBean(String name) {
		return (T) applicationContext.getBean(name);
	}
	
}