package net.ouska.dao;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public interface Expression {

	public String render(List<Object> params);
	
	public static Expression eq(String property, Object value) {
		return simpleExpression(property, "=", value);
	}

	public static Expression neq(String property, Object value) {
		return simpleExpression(property, "!=", value);
	}
	
	public static Expression ge(String property, Object value) {
		return simpleExpression(property, ">=", value);
	}
	
	public static Expression gt(String property, Object value) {
		return simpleExpression(property, ">", value);
	}

	public static Expression le(String property, Object value) {
		return simpleExpression(property, "<=", value);
	}
	
	public static Expression lt(String property, Object value) {
		return simpleExpression(property, "<", value);
	}

	public static Expression isNotNull(String property) {
		requireNotBlankProperty(property);
		return params -> property + " IS NOT NULL ";
	}

	public static Expression isNull(String property) {
		requireNotBlankProperty(property);
		return params -> property + " IS NULL ";
	}

	public static Expression bool(String property, boolean value) {
		requireNotBlankProperty(property);
		return params -> "COALESCE(" + property + ", false) = " + addNamedParam(value, params);
	}

	public static Expression isTrue(String property) {
		requireNotBlankProperty(property);
		return params -> "COALESCE(" + property + ", false) = true";
	}

	public static Expression isFalse(String property) {
		requireNotBlankProperty(property);
		return params -> "COALESCE(" + property + ", false) = false";
	}
	
	public static Expression like(String property, String value) {
		requireNotBlankProperty(property);
		requireNotBlankValue(value);
		return simpleExpression(property, " LIKE ", escape(value));
	}
	
	public static Expression likeIgnoringCase(String property, String value) {
		requireNotBlankProperty(property);
		requireNotBlankValue(value);
		return params -> "LOWER(" + property + ")" + " LIKE LOWER("+addNamedParam(escape(value), params)+")";
	}

	public static Expression startsWith(String property, String value) {
		requireNotBlankProperty(property);
		requireNotBlankValue(value);
		return params -> "LOWER(" + property + ")" + " LIKE " + addNamedParam(escape(value)+"%", params);
	}

	public static Expression startsWithIgnoringCase(String property, String value) {
		requireNotBlankProperty(property);
		requireNotBlankValue(value);
		return params -> "LOWER(" + property + ")" + " LIKE LOWER("+addNamedParam(escape(value)+"%", params)+")";
	}

	public static Expression endsWith(String property, String value) {
		requireNotBlankProperty(property);
		requireNotBlankValue(value);
		return params -> "LOWER(" + property + ")" + " LIKE " + addNamedParam("%"+escape(value), params);
	}

	public static Expression endsWithIgnoringCase(String property, String value) {
		requireNotBlankProperty(property);
		requireNotBlankValue(value);
		return params -> "LOWER(" + property + ")" + " LIKE LOWER("+addNamedParam("%"+escape(value), params)+")";
	}

	public static Expression contains(String property, String value) {
		requireNotBlankProperty(property);
		requireNotBlankValue(value);
		return params -> "LOWER(" + property + ")" + " LIKE "+addNamedParam("%"+escape(value)+"%", params);
	}

	public static Expression containsIgnoringCase(String property, String value) {
		requireNotBlankProperty(property);
		requireNotBlankValue(value);
		return params -> "LOWER(" + property + ")" + " LIKE LOWER("+addNamedParam("%"+escape(value)+"%", params)+")";
	}

	public static Expression in(String property, Collection<? extends Object> collection) {
		requireNotBlankProperty(property);
		requireNotNullValue(collection);
		return params -> {
			StringBuilder b = new StringBuilder();
			b.append(property);
			b.append(" IN (");
			for (Object object : collection) {
				b.append(addNamedParam(object, params));	
			}
			b.append(")");
			return b.toString();
		};
	}

	public static Expression simpleExpression(String property, String operator, Object value) {
		requireNotBlankProperty(property);
		requireNotNullValue(value);
		return new Expression() {
			public String render(List<Object> params) {
				Objects.requireNonNull(params, "Parameters must not be null");
				return property + operator + addNamedParam(value, params);
			}
		};
	}
	
	public static String addNamedParam(Object paramValue, List<Object> params) {
		requireNotNullValue(paramValue);
		params.add(paramValue);
		return "?" + (params.size());
	}

	public static void requireNotBlankProperty(String property) {
		Objects.requireNonNull(property);
		if (Strings.isBlank(property)) {
			throw new IllegalArgumentException("Property must no be null");
		}
	}
	
	public static void requireNotBlankValue(String value) {
		Objects.requireNonNull(value);
		if (Strings.isBlank(value)) {
			throw new IllegalArgumentException("Value must no be null");
		}
	}

	public static void requireNotNullValue(Object value) {
		Objects.requireNonNull(value, "Value must no be null");
	}

	public static String escape(String value) {
		value = value.replace("_", "\\_");
		value = value.replace("%", "\\%");
		return value;
	}
}
