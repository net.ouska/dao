package net.ouska.dao;

public class Selection  {

	String select;
	String from;
	
	public Selection select(String selection) {
		this.select = selection;
		return this;
	}
	public Selection from(String from) {
		this.from = from;
		return this;
	}

}
