package net.ouska.dao;

import java.io.Serializable;

public interface Domain extends Serializable {

	public Long getId();
	
	public void setId(Long id);
	
	public Object getPrimaryKey();
	
	public Class<? extends Domain> getDomainClass();
	
}
