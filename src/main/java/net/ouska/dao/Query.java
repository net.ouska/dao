package net.ouska.dao;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;


public abstract class Query<T, Q extends Query<T,?>> {
	
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";

	protected List<Object> params = new ArrayList<Object>();
	
	private StringBuilder query = new StringBuilder();
	private LogicalOperation nextOperation = LogicalOperation.nope();
	
	private String selectString;
	private String fromString;
	private StringBuilder joinsString = new StringBuilder();
	private StringBuilder orderString = new StringBuilder();
	private DAO dao;
	
	private Class<T> cachedClass = null;
	
	
	@SuppressWarnings("unchecked")
	protected Class<T> getDomainClass() {
		if (cachedClass == null) {
			cachedClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		}
		return cachedClass;
	}

	/**
	 * Method that uses reflection for obtaining default table and var name like
	 * select user from User user
	 * It can be Override if necessary  - especially when adding some join columns 
	 */
	protected void generateSelection() {
		Class<T> domainClass = getDomainClass();
		String tableName = domainClass.getSimpleName();
		String varName = tableName.substring(0, 1).toLowerCase() + tableName.substring(1);
		
		select(varName)
		.from(tableName + " " + varName);
	}

	public Query() {

	}

	public Query(DAO dao) {
		this.dao = dao;
	}

	public Q useDao(DAO dao) {
		this.dao = dao;
		return me();
	}

	protected Q select(String select) {
		this.selectString = select;
		return me();
	}
	
	protected Q from(String fromString) {
		this.fromString = fromString;
		return me();
	}

	protected Q addJoin(String joinsString) {
		this.joinsString.append(" ");
		this.joinsString.append(joinsString);
		this.joinsString.append(" ");
		return me();
	}

	/************************************************/
	
	@SuppressWarnings("unchecked")
	protected Q me() {
		return (Q) this;
	}

	
	protected Q eq(String property, Object value) {
		return addCondition(Expression.eq(property, value));
	}

	protected Q neq(String property, Object value) {
		return addCondition(Expression.neq(property, value));
	}

	protected Q gt(String property, Object value) {
		return addCondition(Expression.gt(property, value));
	}

	protected Q ge(String property, Object value) {
		return addCondition(Expression.ge(property, value));
	}

	protected Q lt(String property, Object value) {
		return addCondition(Expression.lt(property, value));
	}

	protected Q le(String property, Object value) {
		return addCondition(Expression.le(property, value));
	}

	protected Q in(String property, Collection<? extends Object> collection) {
		return addCondition(Expression.in(property, collection));
	}

	protected Q isNull(String property) {
		return addCondition(Expression.isNull(property));
	}

	protected Q isNotNull(String property) {
		return addCondition(Expression.isNotNull(property));
	}
	
	protected Q bool(String property, boolean value) {
		return addCondition(Expression.bool(property, value));
	}

	protected Q isTrue(String property) {
		return addCondition(Expression.isTrue(property));
	}

	protected Q isFalse(String property) {
		return addCondition(Expression.isFalse(property));
	}

	protected Q like(String property, String value) {
		return addCondition(Expression.like(property, value));
	}

	protected Q likeIgnoringCase(String property, String value) {
		return addCondition(Expression.likeIgnoringCase(property, value));
	}

	protected Q startsWith(String property, String value) {
		return addCondition(Expression.startsWith(property, value));
	}

	protected Q startsWithIgnoringCase(String property, String value) {
		return addCondition(Expression.startsWithIgnoringCase(property, value));
	}

	protected Q endsWith(String property, String value) {
		return addCondition(Expression.endsWith(property, value));
	}

	protected Q endsWithIgnoringCase(String property, String value) {
		return addCondition(Expression.endsWithIgnoringCase(property, value));
	}

	protected Q contains(String property, String value) {
		return addCondition(Expression.contains(property, value));
	}

	protected Q containsIgnoringCase(String property, String value) {
		return addCondition(Expression.containsIgnoringCase(property, value));
	}

	
	/************************************************/

	protected Q addCondition(String property, Object value, Operator operator) {
		switch (operator) {
			case EQ: return eq(property, value);
			case NEQ: return neq(property, value);
			case GT: return gt(property, value);
			case GE: return ge(property, value);
			case LT: return lt(property, value);
			case LE: return le(property, value);
			case BOOL: return bool(property, (Boolean) value);
		default:
			throw new IllegalArgumentException("Unexpected operator " + operator);
		}
	}

	protected Q addCondition(String property, String value, Operator operator) {
		switch (operator) {
			case EQ: return eq(property, value);
			case NEQ: return neq(property, value);			
			case LIKE: return like(property, value);
			case LIKE_IGNORING_CASE: return likeIgnoringCase(property, value);
			case STARTS_WITH: return startsWith(property, value);
			case STARTS_WITH_IGNORING_CASE: return startsWithIgnoringCase(property, value);
			case ENDS_WITH: return endsWith(property, value);
			case ENDS_WITH_IGNORING_CASE: return endsWithIgnoringCase(property, value);
			case CONTAINS: return contains(property, value);
			case CONTAINS_IGNORING_CASE_WITH: return containsIgnoringCase(property, value);
		default:
			throw new IllegalArgumentException("Unexpected operator " + operator);
		}
	}

	private void indent() {
		if (DAO.format) {
			for (int i = 0; i < indentCount; i++) {
				query.append("\t");	
			}
		}
	}

	private void newLine() {
		if (DAO.format) {
			boolean appendNewNewLine = true;
			if (query.length() > 0) {
				char[] lastChar = new char[1];
				query.getChars(query.length()-1, query.length(), lastChar, 0);
				appendNewNewLine = lastChar[0] != '\n';
			}   
			if (appendNewNewLine) {
				query.append("\n");	
			}
		}
	}

	protected Q addCondition(Expression exp) {
		indent();
		query.append(nextOperation.apply());
		query.append(exp.render(params));
		newLine();
		nextOperation = LogicalOperation.and();
		return me();
	}

	protected Q addCondition(String generalSQLEpxpression) {
		// In this case params must be handled by calling method something like add("user.id = " + Express.addNamedParam(userId));
		return addCondition(params -> generalSQLEpxpression);
	}

	/************************************************/

	private int indentCount = 0;
	
	private Q applyOperation(LogicalOperation operation, LogicalOperation nextOperation) {
		query.append(operation.apply());
		this.nextOperation = nextOperation;
		return me();
	}

	public Q AND() {
		indent();
		return applyOperation(LogicalOperation.and(), LogicalOperation.nope());
	}
	
	public Q OR() {
		indent();
		return applyOperation(LogicalOperation.or(), LogicalOperation.nope());
	}

	public Q NOT() {
		indent();
		return applyOperation(LogicalOperation.not(), LogicalOperation.nope());
	}

	public Q OPEN() {
		query.append(this.nextOperation.apply());
		newLine();
		indent();
		
		Q f = applyOperation(LogicalOperation.openBlock(), LogicalOperation.nope());
		newLine();

		indentCount++;
		return f; 
	}

	public Q CLOSE() {
		indentCount--;
		indent();
		Q f = applyOperation(LogicalOperation.closeBlock(), LogicalOperation.and());
		newLine();
		return f; 
	}
	
	/************************************************/

	public <O> Q onlyIf(boolean condition, O object, BiConsumer<Q, O> consumer) {
		if (condition) {
			consumer.accept(me(), object);
		}
		return me();
	}

	public Q onlyIf(boolean condition, Consumer<Q> consumer) {
		if (condition) {
			consumer.accept(me());
		}
		return me();
	}

	public Q onlyIf(boolean condition, Runnable runnable) {
		if (condition) {
			runnable.run();
		}
		return me();
	}

	
	public <O> Q onlyIfNotNull(O object, BiConsumer<Q, O> consumer) {
		return onlyIf(object != null, object, consumer);
	}

	public Q onlyIfNotNull(Object object, Consumer<Q> consumer) {
		return onlyIf(object != null, consumer);
	}

	public Q onlyIfNotNull(Object object, Runnable runnable) {
		return onlyIf(object != null, runnable);
	}

	public <O> Q onlyIfNotBlank(String string, BiConsumer<Q, String> consumer) {
		return onlyIf(string != null && !Strings.isBlank(string), string, consumer);
	}

	public Q onlyIfNotBlank(String string, Consumer<Q> consumer) {
		return onlyIf(string != null && !Strings.isBlank(string), consumer);
	}

	public Q onlyIfNotBlank(String string, Runnable runnable) {
		return onlyIf(string != null && !Strings.isBlank(string), runnable);
	}

	public <OBJ> Q ifNotEmpty(Collection<? extends OBJ> collection, BiConsumer<Q, Collection<? extends OBJ>> consumer) {
		return onlyIf(collection != null && !collection.isEmpty(), collection, consumer);
	}
	
	public <OBJ> Q ifNotEmpty(Collection<? extends OBJ> collection, Consumer<Q> consumer) {
		return onlyIf(collection != null && !collection.isEmpty(), consumer);
	}

	public <OBJ> Q ifNotEmpty(Collection<? extends OBJ> collection, Runnable runnable) {
		return onlyIf(collection != null && !collection.isEmpty(), runnable);
	}
	
	/************************************************/
	
	protected String generateQuery(boolean onlyCount) {
		StringBuilder b = new StringBuilder();
		generateSelection();
		b.append("SELECT ");
		if (onlyCount) {
			b.append("count("+selectString+")");
		} else {
			b.append(selectString);	
		}
		b.append(" FROM ");
		b.append(fromString);
		b.append(" ");
		if (!onlyCount ) {
			b.append(joinsString);	
		}
		if (query.length() > 0) {
			b.append(" WHERE ");
			//b.append("\n");
		}
		return b.toString();
	}

	protected String generateFiltering() {
		return query.toString();
	}

	protected String generateOrdering() {
		StringBuilder b = new StringBuilder();
		if (orderString.length() != 0) {
			b.append(" order by ");
			b.append(orderString.toString());
		}
		return b.toString();
	}

	public String getOrderString() {
		return orderString.toString();
	}

	public Q setOrderString(String sortString) {
		this.orderString.setLength(0);
		this.orderString.append(sortString);
		return me();
	}

	public Q orderDesc() {
		orderString.append(" ");
		orderString.append(DESC);
		return me();
	}

	public Q orderAsc() {
		orderString.append(" ");
		orderString.append(ASC);
		return me();
	}

	public Q orderBy(String propertyName) {
		if (orderString.length() > 0) {
			orderString.append(", ");
		}
		orderString.append(propertyName);
		return me();
	}

	public Q orderBy(String property, String order) {
		orderBy(property);
		orderString.append(" ");
		orderString.append(order);
		return me();
	}
	
	/**************************************************************/
	
	protected void ensureDAO() {
		if (dao == null) {
			dao = DAO.instance();
		}
		Objects.requireNonNull(dao, "DAO object cannot be bound to this query");
	}
	
	public List<T> select() {
		ensureDAO();
		return dao.findByQuery(this);	
	}

	public Optional<T> selectUnique() {
		ensureDAO();
		return dao.findUnique(this);	
	}

	public Page<T> select(PageRequest pageRequest) {
		ensureDAO();
		return dao.findByQuery(this, pageRequest);
	}

	public long selectCount() {
		ensureDAO();
		return dao.countByQuery(this);
	}

	@Override
	public String toString() {
		String query = generateQuery(false);
		String filtering = generateFiltering();
		String ordering = generateOrdering();
		if (DAO.debug) {
			for (int i = 0; i < params.size(); i++) {
				String value = params.get(i).toString();
				if (params.get(i) instanceof CharSequence) {
					value = "\"" + value + "\"";
				}
				filtering = filtering.replaceFirst("\\?"+(i+1), value+"))");
			}
		}
		
		return new StringBuilder()
			.append(query)
			.append(" ")
			.append(filtering)
			.append(" ")
			.append(ordering)
			.toString();
	}
}

