package net.ouska.dao;

import static org.junit.jupiter.api.Assertions.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;
import net.ouska.dao.DAO;
import net.ouska.dao.Domain;
import net.ouska.dao.Operator;
import net.ouska.dao.Query;


@Slf4j
public class QueryTestInvoker {
    
    public static <T extends Query<T,?>> void test(DAO dao, Method method) {
    	test(dao, method, null);
    }
    
    @SuppressWarnings({"unchecked", "rawtypes"})
	public static <T extends Query<T,?>> void test(DAO dao, Method method, ArgumentSupplier supplier) {
    	Class<T> clazz = (Class<T>) method.getDeclaringClass();
		Query query = dao.query(clazz);

    	try {
        	Object[] params = new Object[method.getParameterCount()]; 
        	for (int i = 0; i < method.getParameters().length; i++) {
        		Parameter p = method.getParameters()[i];
        		Class<?> type = p.getType();
        		Type parametrizedType = p.getParameterizedType();
        		if (type.equals(String.class)) {
        			params[i] = "TEST_STRING";
        		} else if (type.equals(Operator.class)) {
        			params[i] = Operator.EQ;
        		} else if (type.equals(Long.class)) {
        			params[i] = Long.valueOf(1);
        		} else if (type.equals(Integer.class)) {
        			params[i] = Integer.valueOf(1);
        		} else if (type.equals(Boolean.class)) {
        			params[i] = Boolean.valueOf(true);
        		} else if (type.equals(boolean.class)) {
        			params[i] = true;
        		} else if (Domain.class.isAssignableFrom(type)) {	
        			Domain domain = (Domain) type.getDeclaredConstructor().newInstance();
        			domain.setId(1L);
        			params[i] = domain;
        		} else if (type.equals(Set.class)) {
        			//TODO Better
        			String typeName = parametrizedType.getTypeName();
        			Set set = new HashSet();
        			if (typeName.contains("java.lang.Long" )) {
        				set.add(1L);	
        			} else if (typeName.contains("java.lang.Integer" )) {
        				set.add(1);
        			} else if (typeName.contains("java.lang.String")) {
        				set.add("TEST_STRING");
    				}
        			params[i] = set;
        		} else {
        			if (supplier != null) {
        				params[i] = supplier.supply(p);
        			} else {
        				params[i] = null;	
        			}
        		}
			}
        	log.info("Invoking test method " + method.getName() + " using "  + params);
			method.invoke(query, params);
		} catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InstantiationException e) {
			fail("Error while calling query method", e);
		} catch (InvocationTargetException e) {
			fail("Error while executing method", e);
		}
    	
    	query.select();
    	
   }

}
