package net.ouska.dao;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import lombok.extern.slf4j.Slf4j;
import net.ouska.dao.ArrayIterable;
import net.ouska.dao.Query;

@Slf4j
public class QueryMethodArgumentsProvider  implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
    	
    	Method m = context.getTestMethod().get();
    	QueryTest tq = m.getAnnotation(QueryTest.class);
    	Class<? extends Query<?,?>> queryClass = tq.value();
    	
    	List<Arguments> args = new ArrayList<Arguments>();
    	Set<String> ignoredMethods = ArrayIterable.toSet(tq.ignoredMethods());
    	ignoredMethods.add("generateSelection");
    	
    	for (Method method : ArrayIterable.toList(queryClass.getDeclaredMethods())) {
    		if (ignoredMethods.contains(method.getName())) {
    			continue;
    		}
    		StringBuilder b = new StringBuilder();
    		b.append(method.getDeclaringClass().getSimpleName());
    		b.append(".");
    		b.append(method.getName());
    		b.append("(");
    		StringBuilder bp = new StringBuilder();
    		for (Parameter param : method.getParameters()) {
    			if (bp.length()>0) {
    				bp.append(", ");	
    			}
    			bp.append(param.getType().getSimpleName());
    			bp.append(" ");
    			bp.append(param.getName());
			}
    		b.append(bp);
    		b.append(")");
    		
    		args.add( Arguments.of(method, b.toString()));
		}
    	
    	return args.stream();
    }
}	
