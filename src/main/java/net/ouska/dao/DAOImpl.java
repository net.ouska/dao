package net.ouska.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

//https://www.baeldung.com/jpa-hibernate-persistence-context
//https://mail.codejava.net/frameworks/spring-boot/spring-data-jpa-entitymanager-examples
//https://stackoverflow.com/questions/10394857/how-to-use-transactional-with-spring-data

@Repository(DAO.BEAN_NAME)
public class DAOImpl implements DAO {

	Logger logger = LoggerFactory.getLogger(DAOImpl.class);
	
	@PersistenceContext
	private final EntityManager em;
	
	public DAOImpl(EntityManager entityManager) {
		this.em = entityManager;
	}

	@Transactional
	public <T> void delete(T entity) {
		em.remove(entity);
	}

	@Transactional
	public <T> T create(T entity) {
		em.persist(entity);
		return entity;
	}

	@Transactional
	public <T> T update(T entity) {
		return em.merge(entity);
	}

	@Transactional(readOnly=true)
	public <T> T findById(Class<T> clazz, Long id) {
		T res = em.find(clazz, id);
		if (res == null) {
			String errMessage = "Unable to find "+clazz+" with ID "+id;
			logger.warn(errMessage);
			throw new RuntimeException(errMessage);
		}
		return res;
	}

	@Transactional(readOnly=true)
	public <T> Optional<T> find(Class<T> clazz, Long id) {
		T res = em.find(clazz, id);
		if (res == null) {
			return Optional.empty();
		}
		return Optional.of(res);
	}

	@Transactional(readOnly=true)
	public <T> List<T> findAll(Class<T> clazz) {
		Session session = em.unwrap(Session.class);
		org.hibernate.query.Query<T> user = session.createQuery("from " + clazz.getSimpleName(), clazz);
		return user.getResultList();
	}

	@Transactional(readOnly=true)
	public <T, Q extends Query<T,?>> long countByQuery(Query<T, Q> query) {
		List<Object> params = new ArrayList<Object>();
		StringBuilder b = new StringBuilder();
		b.append(query.generateQuery(true));
		b.append(query.generateFiltering());
		String select = b.toString();
		Session session = em.unwrap(Session.class);
		logger.info("HQL: " + select);
		org.hibernate.query.Query<T> q = session.createQuery(select, query.getDomainClass());
		org.hibernate.query.Query<Long> qCount = session.createQuery(select, Long.class);
		for (int i = 0; i < params.size(); i++) {
			q.setParameter(i+1, params.get(i));	
		}
		return qCount.getSingleResult();
	}

	@Transactional(readOnly=true)
	public <T, Q extends Query<T,?>> Optional<T> findUnique(Query<T, Q> query) {
		List<T> items = findByQuery(query, null).getPageItems();
		if (items.size() == 0) {
			return Optional.empty();
		} else if (items.size() == 1) {
			return Optional.of(items.get(0));
		} else {
			throw new RuntimeException("Not unique result");
		}
	}

	@Transactional(readOnly=true)
	public <T, Q extends Query<T,?>> List<T> findByQuery(Query<T, Q> query) {
		return findByQuery(query, null).getPageItems();
	}

	@Transactional(readOnly=true)
	public <T, Q extends Query<T,?>> Page<T> findByQuery(Query<T, Q> query, PageRequest pageRequest) {

		List<Object> params = query.params;
		String selectNonCount = query.generateQuery(false);
		String selectFiltering = query.generateFiltering();
		String selectSorting = query.generateOrdering();
		
		String select = selectNonCount + selectFiltering + selectSorting;
		
		Session session = em.unwrap(Session.class);
		logger.info("HQL: " + select);
		org.hibernate.query.Query<T> q = session.createQuery(select, query.getDomainClass());
		
		if (pageRequest != null) {
			q.setMaxResults(pageRequest.getPageSize());
			q.setFirstResult(pageRequest.getPageNumber() * pageRequest.getPageSize());		
		}
		
		for (int i = 0; i < params.size(); i++) {
			q.setParameter(i+1, params.get(i));	
		}
		List<T> values = q.getResultList();
		
		Page<T> page = new Page<T>();
		page.setPageItems(values);
		page.setPageNumber(0);
		page.setPageSize(values.size());
		page.setTotalCount(values.size());
		page.setTotalPages(1);
		
		if (pageRequest != null) {
			//params = new ArrayList<Object>();
			String selectCount = query.generateQuery(true);
			select = selectCount + selectFiltering;

			org.hibernate.query.Query<Long> qCount = session.createQuery(select, Long.class);
			for (int i = 0; i < params.size(); i++) {
				qCount.setParameter(i+1, params.get(i));	
			}
			Long totalCount = qCount.getSingleResult();
			
			page.setPageNumber(pageRequest.getPageNumber());
			page.setPageSize(pageRequest.getPageSize());
			page.setTotalCount(totalCount);
			page.setTotalPages((totalCount / pageRequest.getPageSize()) + 1);
		}
		
		return page;
	}	
	
	@Transactional(readOnly=true)
	public <D, Q extends Query<D,?>> Q query(Class<Q> clazz) {
		try {
			Q query = clazz.getDeclaredConstructor().newInstance();
			query.useDao(this);
			return query;
		} catch (Exception e) {
			throw new RuntimeException("Cannot create instance of Domain Query " + clazz.getName());
		}
	}
	
	@Transactional(readOnly=true)
	public <T> List<T> select(String select, List<Object> params, Class<T> resultClass) {
		Session session = em.unwrap(Session.class);
		logger.info("HQL: " + select);
		org.hibernate.query.Query<T> q = session.createQuery(select, resultClass);
		for (int i = 0; i < params.size(); i++) {
			q.setParameter(i+1, params.get(i));	
		}
		return q.getResultList();
	}
}
