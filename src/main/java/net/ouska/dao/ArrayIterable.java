package net.ouska.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class ArrayIterable<T extends Object> implements Iterable<T> {

	private final T[] source;
	
	public ArrayIterable(T[] source) {
		this.source = source;
	}

	public static <R extends Object> ArrayIterable<R> of(R[] source) {
		return new ArrayIterable(source);
	}

	public static <R extends Object> List<R> toList(R[] source) {
		List<R> list = new ArrayList<R>();
		for (R item : of(source)) {
			list.add(item);
		}
		return list;
	}

	public static <R extends Object> Set<R> toSet(R[] source) {
		Set<R> set = new HashSet<R>();
		for (R item : of(source)) {
			set.add(item);
		}
		return set;
	}

	public static <R extends Object> Optional<R> first(R[] source) {
		if (source != null && source.length > 0) {
			return Optional.of(source[0]);
		} else {
			return Optional.empty();
		}
	}

    @Override
    public Iterator<T> iterator() {
    	return new ArrayIterator<T>(source); 
    }
    
    private static class ArrayIterator<T> implements Iterator<T> {
    	private final T[] source;
    	private int index;
    	
    	public ArrayIterator(T[] source) {
			this.source = source;
		}

		@Override
		public boolean hasNext() {
			return index < source.length;
		}

		@Override
		public T next() {
			return source[index++];
		}
    }
}
