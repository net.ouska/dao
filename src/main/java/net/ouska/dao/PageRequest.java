package net.ouska.dao;

public class PageRequest {

	int pageNumber; 
	int pageSize;

	public static PageRequest of(int pageNumber, int pageSize) {
		return new PageRequest(pageNumber, pageSize);
	}

	public PageRequest(int pageNumber, int pageSize) {
		super();
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
	}
	
	public int getPageNumber() {
		return pageNumber;
	}
	
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	public int getPageSize() {
		return pageSize;
	}
	
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
