package net.ouska.dao;

import java.lang.reflect.Parameter;

public interface ArgumentSupplier {
	   
	   public Object supply(Parameter p);

}
