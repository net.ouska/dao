package net.ouska.dao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import net.ouska.dao.Query;



@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@ParameterizedTest(name="{index} {1}")
@ArgumentsSource(QueryMethodArgumentsProvider.class)
public @interface QueryTest {
	
	Class<? extends Query<?,?>> value();
	
	String[] ignoredMethods() default "";
	
}
