package net.ouska.dao;

import java.util.List;
import java.util.Optional;


public interface DAO {

	public static final String BEAN_NAME = "DAO";
	
	public static boolean debug = false;
	public static boolean format = false;
	
	public <T> void delete(T entity);

	public <T> T create(T entity);

	public <T> T update(T entity);

	public <T> T findById(Class<T> clazz, Long id);

	public <T> Optional<T> find(Class<T> clazz, Long id);

	public <T> List<T> findAll(Class<T> clazz);

	public <T, Q extends Query<T,?>> long countByQuery(Query<T, Q> query);

	public <T, Q extends Query<T,?>> Optional<T> findUnique(Query<T, Q> query);

	public <T, Q extends Query<T,?>> List<T> findByQuery(Query<T, Q> query);
	
	public <T, Q extends Query<T,?>> Page<T> findByQuery(Query<T, Q> query, PageRequest pageRequest);	
	
	public <D, Q extends Query<D,?>> Q query(Class<Q> clazz);
	
	public <T> List<T> select(String select, List<Object> params, Class<T> resultClass);
	
	public static DAO instance() {
		return AppContext.getBean(BEAN_NAME);
	}	
	
}
